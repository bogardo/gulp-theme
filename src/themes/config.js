module.exports = [
    {
        name: 'main',
        file: 'main.scss'
    },
    {
        name: 'secondary',
        file: 'secondary.scss'
    },
    {
        name: 'tertiary',
        file: 'tertiary.scss'
    }
];
