var themes = require('./src/themes/config.js');

var gulp = require('gulp');
var sass = require('gulp-sass');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

// backup original/placeholder theme file
gulp.task('theme:backup', function(){
    return gulp.src('./src/scss/_theme.scss')
        .pipe(rename('original_theme.scss'))
        .pipe(gulp.dest('./src/scss'));
});

// loop through the themes
themes.forEach((theme) => {

    // move theme file to src directory
    gulp.task('theme:move:' + theme.name, function(){
        return gulp.src('./src/themes/' + theme.file)
            .pipe(rename('_theme.scss'))
            .pipe(gulp.dest('./src/scss'));
    });

    // parse scss
    gulp.task('theme:scss:' + theme.name, function(){
        return gulp.src('./src/scss/**/*.scss')
                .pipe(sass())
                .pipe(concat(theme.name + '.scss'))
                .pipe(gulp.dest('./dist/css'))
    });

});

// restore original/placeholder theme file
gulp.task('theme:restore', function(){
    return gulp.src('./src/scss/original_theme.scss')
        .pipe(clean({force: true}))
        .pipe(rename('_theme.scss'))
        .pipe(gulp.dest('./src/scss', {overwrite: true}));
});


var serie = ['theme:backup'];
serie = themes.reduce((value, theme, index, themes) => {
    value.push('theme:move:'+theme.name);
    value.push('theme:scss:'+theme.name);
    return value;
}, serie);
serie.push('theme:restore');
serie.push(function(done){
    done();
});

gulp.task('themes', gulp.series(serie));

gulp.task('default', gulp.series('themes'))
